package pl.sda.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.sda.TestUtil;
import pl.sda.configuration.PropertyReader;
import pl.sda.domain.Employee;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class EmployeeDaoJdbcImplTest {

    private EmployeeDao empDAO;

    @BeforeEach
    void init() throws IOException, SQLException {
        JdbcConnectionManager jdbcConnectionManager = new JdbcConnectionManager(PropertyReader.loadConfiguration());
        empDAO = new EmployeeDaoJdbcImpl(jdbcConnectionManager);
        TestUtil.cleanUpDatabase(jdbcConnectionManager);
    }

    @Test
    void findById() throws Exception {
        Employee employee = empDAO.findById(7369);
        assertThat(employee).isNotNull();
        assertThat(employee.getEmployeeNo()).isEqualTo(7369);
        assertThat(employee.getEmployeeName()).isEqualTo("SMITH");
        assertThat(employee.getJob()).isEqualTo("CLERK");
        assertThat(employee.getSalary()).isEqualTo(800);
    }

    @Test
    void create() throws Exception {
        Employee employee = new Employee(3, "LAMORSKA", "TESTER", 7566, LocalDate.now(), 12345F, 1F, 20);

        empDAO.create(employee);

        Employee employeeFromDb = empDAO.findById(3);

        assertThat(employeeFromDb).isNotNull();
        assertThat(employeeFromDb.getEmployeeNo()).isEqualTo(employee.getEmployeeNo());
        assertThat(employeeFromDb.getEmployeeName()).isEqualTo(employee.getEmployeeName());
        assertThat(employeeFromDb.getSalary()).isEqualTo(employee.getSalary());
        assertThat(employeeFromDb.getHiredate()).isEqualTo(employee.getHiredate());
    }

    @Test
    void update() throws Exception {
        Employee employee = empDAO.findById(7654);
        assertThat(employee).isNotNull();

        employee.setEmployeeName("NEW_EMP");
        empDAO.update(employee);

        employee = empDAO.findById(7654);

        assertThat(employee).isNotNull();
        assertThat(employee.getEmployeeNo()).isEqualTo(7654);
        assertThat(employee.getEmployeeName()).isEqualTo("NEW_EMP");
        assertThat(employee.getSalary()).isEqualTo(1250);
        assertThat(employee.getHiredate()).isEqualTo("1998-12-05");
    }

    @Test
    void delete() throws Exception {
        Employee employee = empDAO.findById(7369);
        assertThat(employee).isNotNull();

        empDAO.delete(7369);

        employee = empDAO.findById(7369);
        assertThat(employee).isNull();
    }

    @Test
    void createMultipleEmployeesAllOk() throws Exception {
        List<Employee> employeeList = new ArrayList<>();

        Employee employee1 = new Employee(1, "LAMORSKA", "TESTER", 7698, LocalDate.now(), 12345F, 1F, 20);
        Employee employee2 = new Employee(2, "ASDF", "COOK", 7698, LocalDate.of(2000, 4, 12), 2000F, 1F, 20);
        Employee employee3 = new Employee(3, "QWERTY", "DRIVER", 7698, LocalDate.of(1999, 12, 31), 10_000F, 1F, 20);
        Employee employee4 = new Employee(4, "ABCD", "BOOK KEEPER", 7698, LocalDate.now(), 35_000F, 1F, 20);

        employeeList.add(employee1);
        employeeList.add(employee2);
        employeeList.add(employee3);
        employeeList.add(employee4);

        empDAO.create(employeeList);

        Employee employeeFromDb1 = empDAO.findById(1);
        Employee employeeFromDb2 = empDAO.findById(2);
        Employee employeeFromDb3 = empDAO.findById(3);
        Employee employeeFromDb4 = empDAO.findById(4);

        assertThat(employeeFromDb1).isNotNull();
        assertThat(employeeFromDb2).isNotNull();
        assertThat(employeeFromDb3).isNotNull();
        assertThat(employeeFromDb4).isNotNull();
        assertThat(employeeFromDb1.getEmployeeNo()).isEqualTo(employee1.getEmployeeNo());
        assertThat(employeeFromDb2.getEmployeeName()).isEqualTo(employee2.getEmployeeName());
        assertThat(employeeFromDb3.getSalary()).isEqualTo(employee3.getSalary());
        assertThat(employeeFromDb4.getHiredate()).isEqualTo(employee4.getHiredate());

    }

    @Test
    void createMultipleEmployeesSecondRowFail() throws Exception {
        List<Employee> employeeList = new ArrayList<>();

        Employee employee1 = new Employee(1, "LAMORSKA", "TESTER", 7698, LocalDate.now(), 12345F, 1F, 20);
        Employee employee2 = new Employee(2, "ASDF", "COOK", 7698, LocalDate.of(2000, 4, 12), 2000F, 1F, 20);
        Employee employee3 = new Employee(2, "QWERTY", "DRIVER", 7698, LocalDate.of(1999, 12, 31), 10_000F, 1F, 20);
        Employee employee4 = new Employee(4, "ABCD", "BOOK KEEPER", 7698, LocalDate.now(), 35_000F, 1F, 20);

        employeeList.add(employee1);
        employeeList.add(employee2);
        employeeList.add(employee3);
        employeeList.add(employee4);

        empDAO.create(employeeList);

        Employee employeeFromDb1 = empDAO.findById(1);
        Employee employeeFromDb2 = empDAO.findById(2);
        Employee employeeFromDb3 = empDAO.findById(3);
        Employee employeeFromDb4 = empDAO.findById(4);

        assertThat(employeeFromDb1).isNotNull();
        assertThat(employeeFromDb2).isNotNull();
        assertThat(employeeFromDb3).isNull();
        assertThat(employeeFromDb4).isNull();
        assertThat(employeeFromDb1.getEmployeeNo()).isEqualTo(employee1.getEmployeeNo());
        assertThat(employeeFromDb2.getEmployeeName()).isEqualTo(employee2.getEmployeeName());
    }

    @Test
    void getTotalSalaryByDept() throws Exception {
        Float salary = empDAO.getTotalSalaryByDept(30);

        assertThat(salary).isEqualTo(9400);

    }
}