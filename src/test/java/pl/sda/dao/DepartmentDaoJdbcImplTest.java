package pl.sda.dao;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.sda.TestUtil;
import pl.sda.configuration.PropertyReader;
import pl.sda.domain.Department;

import java.io.IOException;
import java.sql.SQLException;

import static org.assertj.core.api.Assertions.assertThat;

class DepartmentDaoJdbcImplTest {
    private DepartmentDao deptDAO;

    @BeforeEach
    void init() throws IOException, SQLException {
        JdbcConnectionManager jdbcConnectionManager = new JdbcConnectionManager(PropertyReader.loadConfiguration());
        deptDAO = new DepartmentDaoJdbcImpl(jdbcConnectionManager);
        TestUtil.cleanUpDatabase(jdbcConnectionManager);
    }

    @Test
    void findById() throws Exception {
        Department department = deptDAO.findById(10);
        assertThat(department).isNotNull();
        assertThat(department.getDepartmentNo()).isEqualTo(10);
        assertThat(department.getDepartmentName()).isEqualTo("Accounting");
        assertThat(department.getLocation()).isEqualTo("New York");
    }

    @Test
    void create() throws Exception {
        Department department = new Department(99, "HR", "Las Vegas");

        deptDAO.create(department);

        Department departmentFromDb = deptDAO.findById(99);

        assertThat(departmentFromDb).isNotNull();
        assertThat(departmentFromDb.getDepartmentNo()).isEqualTo(department.getDepartmentNo());
        assertThat(departmentFromDb.getDepartmentName()).isEqualTo(department.getDepartmentName());
        assertThat(departmentFromDb.getLocation()).isEqualTo(department.getLocation());
    }

    @Test
    void update() throws Exception {
        Department department = deptDAO.findById(10);
        assertThat(department).isNotNull();

        department.setDepartmentName("NEW_DEPT");
        deptDAO.update(department);

        department = deptDAO.findById(10);

        assertThat(department).isNotNull();
        assertThat(department.getDepartmentNo()).isEqualTo(10);
        assertThat(department.getDepartmentName()).isEqualTo("NEW_DEPT");
        assertThat(department.getLocation()).isEqualTo("New York");
    }

    @Test
    void delete() throws Exception {
        Department department = deptDAO.findById(40);
        assertThat(department).isNotNull();

        deptDAO.delete(40);

        department = deptDAO.findById(40);
        assertThat(department).isNull();
    }
}
