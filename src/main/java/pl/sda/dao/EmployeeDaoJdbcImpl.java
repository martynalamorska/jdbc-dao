package pl.sda.dao;

import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.sda.domain.Employee;

import java.sql.*;
import java.time.LocalDate;
import java.util.List;

public class EmployeeDaoJdbcImpl implements EmployeeDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentDaoJdbcImpl.class);

    private static String QUERY_BY_ID = "SELECT empno, ename, job, manager, hiredate, salary, commision, deptno FROM emp WHERE empno = ?";
    private static String INSERT_STMT = "INSERT INTO emp(empno, ename, job, manager, hiredate, salary, commision, deptno) VALUES(?,?,?,?,?,?,?,?)";
    private static String UPDATE_STMT = "UPDATE emp set ename = ?, job = ?, manager = ?, hiredate = ?, salary = ?, commision = ?, deptno = ?  WHERE empno = ?";
    private static String DELETE_STMT = "DELETE FROM emp WHERE empno = ?";
    private static String QUERY_TOTAL_SALARY_BY_DEPT = "{call calculate_salary_by_dept(?, ?)}";

    private final JdbcConnectionManager jdbcConnectionManager;

    public EmployeeDaoJdbcImpl(JdbcConnectionManager jdbcConnectionManager) {
        this.jdbcConnectionManager = jdbcConnectionManager;
    }

    @Override
    public Employee findById(int id) {
        try (Connection connection = jdbcConnectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(QUERY_BY_ID)) {

            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                int empno = resultSet.getInt("empno");
                String ename = resultSet.getString("ename");
                String job = resultSet.getString("job");
                Integer manager = resultSet.getInt("manager");
                LocalDate hiredate = resultSet.getObject("hiredate", LocalDate.class);
                Float salary = resultSet.getFloat("salary");
                Float commision = resultSet.getFloat("commision");
                int deptno = resultSet.getInt("deptno");

                return new Employee(empno, ename, job, manager, hiredate, salary, commision, deptno);
            }

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    @Override
    public void create(Employee employee) {
        try (Connection connection = jdbcConnectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_STMT)) {

            preparedStatement.setInt(1, employee.getEmployeeNo());
            preparedStatement.setString(2, employee.getEmployeeName());
            preparedStatement.setString(3, employee.getJob());
            preparedStatement.setInt(4, employee.getManager());
            preparedStatement.setDate(5, Date.valueOf(employee.getHiredate()));
            preparedStatement.setFloat(6, employee.getSalary());
            preparedStatement.setFloat(7, employee.getCommission());
            preparedStatement.setInt(8, employee.getDeptno());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

    }

    @Override
    public void update(Employee employee) {
        try (Connection connection = jdbcConnectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_STMT)) {

            preparedStatement.setInt(8, employee.getEmployeeNo());
            preparedStatement.setString(1, employee.getEmployeeName());
            preparedStatement.setString(2, employee.getJob());
            preparedStatement.setInt(3, employee.getManager());
            preparedStatement.setDate(4, Date.valueOf(employee.getHiredate()));
            preparedStatement.setFloat(5, employee.getSalary());
            preparedStatement.setFloat(6, employee.getCommission());
            preparedStatement.setInt(7, employee.getDeptno());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    public void delete(int id) {
        try (Connection connection = jdbcConnectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_STMT)) {

            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    public void create(List<Employee> employees) throws Exception {
        try (Connection connection = jdbcConnectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_STMT)) {

            for (Employee employee : employees) {
                preparedStatement.setInt(1, employee.getEmployeeNo());
                preparedStatement.setString(2, employee.getEmployeeName());
                preparedStatement.setString(3, employee.getJob());
                preparedStatement.setInt(4, employee.getManager());
                preparedStatement.setDate(5, Date.valueOf(employee.getHiredate()));
                preparedStatement.setFloat(6, employee.getSalary());
                preparedStatement.setFloat(7, employee.getCommission());
                preparedStatement.setInt(8, employee.getDeptno());
                preparedStatement.executeUpdate();
            }

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    public Float getTotalSalaryByDept(int dept) throws Exception {
        try (Connection connection = jdbcConnectionManager.getConnection();
        CallableStatement callableStatement = connection.prepareCall(QUERY_TOTAL_SALARY_BY_DEPT)){
            callableStatement.setInt(1, dept);
            callableStatement.registerOutParameter(2, JDBCType.FLOAT);

            boolean execute = callableStatement.execute();

            return callableStatement.getFloat(2);
        }
    }
}
