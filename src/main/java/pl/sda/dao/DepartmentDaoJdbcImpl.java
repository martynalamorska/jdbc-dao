package pl.sda.dao;

import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.sda.domain.Department;

import java.sql.*;

public class DepartmentDaoJdbcImpl implements DepartmentDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentDaoJdbcImpl.class);

    private static String QUERY_BY_ID = "SELECT deptno, dname, location FROM dept WHERE deptno = ?";
    private static String INSERT_STMT = "INSERT INTO dept(deptno, dname, location) VALUES(?,?,?)";
    private static String UPDATE_STMT = "UPDATE dept set dname = ?, location = ?  WHERE deptno = ?";
    private static String DELETE_STMT = "DELETE FROM dept WHERE deptno = ?";

    private final JdbcConnectionManager jdbcConnectionManager;

    public DepartmentDaoJdbcImpl(final JdbcConnectionManager jdbcConnectionManager) {
        this.jdbcConnectionManager = jdbcConnectionManager;
    }

    @Override
    public Department findById(int id) {
        try (Connection connection = jdbcConnectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(QUERY_BY_ID)) {

            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                int deptno = resultSet.getInt("deptno");
                String dname = resultSet.getString("dname");
                String location = resultSet.getString("location");

                return new Department(deptno, dname, location);
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
        return null;
    }

    @Override
    public void create(Department department) {
        try (Connection connection = jdbcConnectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_STMT)) {

            preparedStatement.setInt(1, department.getDepartmentNo());
            preparedStatement.setString(2, department.getDepartmentName());
            preparedStatement.setString(3, department.getLocation());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }

    @Override
    public void update(Department department) {
        try (Connection connection = jdbcConnectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_STMT)) {

            preparedStatement.setInt(3, department.getDepartmentNo());
            preparedStatement.setString(1, department.getDepartmentName());
            preparedStatement.setString(2, department.getLocation());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }

    }

    @Override
    public void delete(int id) {

        try (Connection connection = jdbcConnectionManager.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(DELETE_STMT)) {

            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.error(e.getMessage());
        }
    }
}
