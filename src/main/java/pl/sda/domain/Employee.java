package pl.sda.domain;

import java.time.LocalDate;
import java.util.Objects;

public class Employee {
    private int employeeNo;
    private String employeeName;
    private String job;
    private Integer manager;
    private LocalDate hiredate;
    private Float salary;
    private Float commission;
    private int deptno;


    public Employee() {
    }

    public Employee(int employeeNo, String employeeName, String job, Integer manager, LocalDate hiredate, Float salary, Float commission, int deptno) {
        this.employeeNo = employeeNo;
        this.employeeName = employeeName;
        this.job = job;
        this.manager = manager;
        this.hiredate = hiredate;
        this.salary = salary;
        this.commission = commission;
        this.deptno = deptno;
    }

    public int getEmployeeNo() {
        return employeeNo;
    }

    public void setEmployeeNo(int employeeNo) {
        this.employeeNo = employeeNo;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public Integer getManager() {
        return manager;
    }

    public void setManager(Integer manager) {
        this.manager = manager;
    }

    public LocalDate getHiredate() {
        return hiredate;
    }

    public void setHiredate(LocalDate hiredate) {
        this.hiredate = hiredate;
    }

    public Float getSalary() {
        return salary;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }

    public Float getCommission() {
        return commission;
    }

    public void setCommission(Float commission) {
        this.commission = commission;
    }

    public int getDeptno() {
        return deptno;
    }

    public void setDeptno(int deptno) {
        this.deptno = deptno;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return employeeNo == employee.employeeNo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(employeeNo);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "employeeNo=" + employeeNo +
                ", employeeName='" + employeeName + '\'' +
                ", job='" + job + '\'' +
                ", manager='" + manager + '\'' +
                ", hiredate=" + hiredate +
                ", salary=" + salary +
                ", commission=" + commission +
                ", deptno=" + deptno +
                '}';
    }
}
