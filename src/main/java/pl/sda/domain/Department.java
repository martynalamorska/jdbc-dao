package pl.sda.domain;

import java.util.Objects;

public class Department {
    private int departmentNo;
    private String departmentName;
    private String location;

    public Department() {
    }

    public Department(int departmentNo, String departmentName, String location) {
        this.departmentNo = departmentNo;
        this.departmentName = departmentName;
        this.location = location;
    }

    public int getDepartmentNo() {
        return departmentNo;
    }

    public void setDepartmentNo(int departmentNo) {
        this.departmentNo = departmentNo;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return departmentNo == that.departmentNo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentNo);
    }

    @Override
    public String toString() {
        return "Department{" +
                "departmentNo=" + departmentNo +
                ", departmentName='" + departmentName + '\'' +
                ", location='" + location + '\'' +
                '}';
    }
}
